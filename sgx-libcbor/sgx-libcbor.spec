# This package only provides SGX enclave content
%define debug_package %{nil}

# We need direct control over build flags used for the
# SGX enclave code.
%undefine _auto_set_build_flags

Name: sgx-libcbor
Version: 0.10.2
Release: 1
Summary: SGX support for CBOR parsing

License: MIT
URL: http://libcbor.org
Source0: https://github.com/PJK/libcbor/archive/v%{version}.tar.gz#/libcbor-%{version}.tar.gz

# From linux-sgx.git/external/cbor/sgx_cbor.patch
Patch1: sgx_cbor.patch

BuildRequires: cmake
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: sgx-srpm-macros
BuildRequires: sgx-enclave-devel

%description
libcbor is a C library for parsing and generating CBOR.

This provides a build for the SGX target environment

%package -n sgx-enclave-libcbor-devel
Summary: SGX support for CBOR parsing

%description -n sgx-enclave-libcbor-devel
libcbor is a C library for parsing and generating CBOR.

This package provides the development libraries and header
fiels.

%prep
%setup -q -n libcbor-%{version}
%patch -P 1 -p1

%build
%sgx_cmake \
  -DSGX_SDK=%{sgx_prefix} \
  -DCMAKE_C_ENCLAVE_FLAGS="%{sgx_enclave_cflags}"

%sgx_cmake_build

%install
%sgx_cmake_install
mv %{buildroot}%{sgx_libdir}/libcbor.a %{buildroot}%{sgx_libdir}/libsgx_cbor.a


%files -n sgx-enclave-libcbor-devel
%{sgx_libdir}/libsgx_cbor.a
%{sgx_libdir}/pkgconfig/libcbor.pc
%{sgx_includedir}/cbor.h
%{sgx_includedir}/cbor/

%changelog
* Mon Jan 29 2024 Daniel P. Berrangé <berrange@redhat.com> - 0.10.2-1
- Initial packaging
