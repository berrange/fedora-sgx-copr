# This package only provides SGX enclave content
%define debug_package %{nil}

# We need direct control over build flags used for the
# SGX enclave code.
#
# Unfortunately this package also builds some native
# code, but he makefiles for native & enclave code
# are so horribly tangled it is difficult to make
# native code honour default RPM CFLAGS while keeping
# those flags out of enclave code
%undefine _auto_set_build_flags

# If set will only build the core C & C++ libraries
# and CLI tools. Otherwise it will also build trts
# and tcrypt, which depend on dcap
%{!?sgx_bootstrap:%define sgx_bootstrap 0}

%global dcap_version 1.19

Name: sgx-sdk
Version: 2.22
Release: 1%{?dist}
Summary: SGX Software Development Kit

License: Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND BSD-4-Clause AND BSD-4-Clause-UC AND ISC AND MIT AND NCSA AND OpenSSL AND SMLNJ
URL: https://github.com/intel/linux-sgx

Source0: https://github.com/intel/linux-sgx/archive/refs/tags/sgx_%{version}.tar.gz#/linux-sgx-sgx_%{version}.tar.gz

Source1: https://download.01.org/intel-sgx/sgx-linux/%{version}/prebuilt_ae_%{version}.tar.gz

# https://github.com/Intel-EPID-SDK/epid-sdk
#
# Upstream is discontinued/archived.
# Last upstream epid-sdk wass 8.0.0
#
# The bundled code is derived from 6.0.0, with
# various downstream changes especially to the
# build system (likely to make it repreoducible)
#
# 8.0.0 had some API changes so it is not clear
# it would be compatible with what SGX neesds
#
#  => continuing to bundle is only practical option
Provides: bundled(epid-sdk) == 6.0.0

# http://software.intel.com/sites/default/files/article/185457/librdrand-windows-r1.zip
#
# There is no upstream project, just the zip file dump.
# There is no shared library, so any usage would be
# static linked regardless.
#
#  => no benefit in unbundling
Provides: bundled(librdrand)

# https://github.com/libunwind/libunwind
#
# This is bundling a version from 2019, which has
# been patched in many places with HAVE_SGX conditions.
#
#  => TBD, but forward porting patches to new version
#     is likely impractical
Provides: bundled(libunwind) = 1.3.1

Patch1: 0001-Use-distro-libcrypto-and-tinyxml2-for-signtool.patch
Patch2: 0002-Use-libsgx_mm.a-from-distro-instead-of-bundled-copy.patch
Patch3: 0003-Use-libsgx_ippcp.a-from-distro-instead-of-bundled-co.patch
Patch4: 0004-Use-distro-provieded-openssl-instead-of-prebuilt-cop.patch
Patch5: 0005-Use-libcbor-from-distro-instead-of-bundled-copy.patch
Patch6: 0006-Use-header-files-from-distro-instead-of-bundled-DCAP.patch
Patch7: 0007-Varisous-tlibc-fixes-for-new-GCC-14.patch
Patch8: 0008-Improve-make-debuggability.patch
Patch9: 0009-Various-cpprt-fixes-for-GCC-14.patch
Patch10: 0010-Avoid-__has_feature-cxx_constexpr_string_builtins-ch.patch

# For bootstrap builds
BuildRequires: sgx-srpm-macros
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: ocaml
BuildRequires: ocaml-ocamlbuild
BuildRequires: openssl-devel
BuildRequires: tinyxml2-devel
BuildRequires: sgx-enclave-emm-devel
BuildRequires: sgx-enclave-dcap-devel = %{dcap_version}

# For full builds
%if %{sgx_bootstrap} == 0
BuildRequires: sgx-enclave-ipp-crypto-devel
BuildRequires: sgx-enclave-ssl-devel
BuildRequires: sgx-enclave-libcbor-devel	
# XXXX patch to use shared library instead
BuildRequires: libcbor-static
BuildRequires: protobuf-compiler
BuildRequires: protobuf-devel
%endif

# SGX is a feature supported and verified on x86_64 only.
ExclusiveArch: x86_64

%description
The Intel SGX SDK is a collection of APIs, libraries, documentations and
tools that allow software developers to create and debug Intel SGX 
enabled applications in C/C++.


%package -n sgx-enclave-devel
Summary: SGX enclave libraries development

%description -n sgx-enclave-devel
This package contains the header files, libraries and tools required
to create SGX enclaves.

%package -n sgx-platform-stubs-devel
Summary: SGX platform stub library development

%description -n sgx-platform-stubs-devel
This package contains the stub libraries for SGX
platform development

%prep
%autosetup -n linux-sgx-sgx_%{version} -p1

# Temporarily preserve pieces we don't un-bundle
mv external/epid-sdk external/rdrand external/vtune .

# Purge everything else to avoid accidentally building
# bundled code
rm -rf external/*
rm -rf sdk/gperftools/

# Restore the pieces we keep bundled
mv epid-sdk rdrand vtune external/

# The pre-built and (critically) signed enclaves
tar zxvf %{SOURCE1}

# XXXX  unbundle ?
#rm -rf sdk/cpprt/linux/libunwind

%build

# XXX Just modify -I to point to original path instead of copying
ln -sf %{sgx_prefix}/include/sgx-emm/sgx_mm.h common/inc/sgx_mm.h
ln -sf %{sgx_prefix}/include/sgx-emm/sgx_mm_primitives.h common/inc/sgx_mm_primitives.h
ln -sf %{sgx_prefix}/include/sgx-emm/sgx_mm_rt_abstraction.h common/inc/sgx_mm_rt_abstraction.h

ln -sf %{sgx_prefix}/include/sgx-emm/bit_array.h common/inc/internal/bit_array.h
ln -sf %{sgx_prefix}/include/sgx-emm/bit_array_imp.h common/inc/internal/bit_array_imp.h
ln -sf %{sgx_prefix}/include/sgx-emm/ema.h common/inc/internal/ema.h
ln -sf %{sgx_prefix}/include/sgx-emm/ema_imp.h common/inc/internal/ema_imp.h
ln -sf %{sgx_prefix}/include/sgx-emm/emm_private.h common/inc/internal/emm_private.h

############################################################
#
# Bootstrap: the building blocks that can be compiled without
#            any significant external dependencies. The basic
#            runtime needed to build other libraries as enclaves
#
############################################################

%make_build -C sdk -j1 \
    tstdc tcxx tservice tprotected_fs uprotected_fs ptrace \
    libcapable sgx_encrypt sgx_uswitchless pthread \
    edger8r signtool


############################################################
#
# Full build: the pieces that depend on other libraries, most
#             especially needing crypto support
#
############################################################

%if %{sgx_bootstrap} == 0

%make_build -C sdk -j1 \
    SGX_SDK=%{sgx_prefix} \
    SGX_IPP_INC=%{sgx_includedir}/ipp \
    IPP_LIBS_DIR=%{sgx_libdir} \
    LIBCBOR=%{_libdir}/libcbor.a \
    LIBSGX_CBOR=%{sgx_libdir}/libsgx_cbor.a \
    trts tcrypto simulation \
    tkey_exchange ukey_exchange sgx_pcl sgx_tswitchless ttls utls

%endif

%install

%__install -d %{buildroot}%{_bindir}
%__install -d %{buildroot}%{_sysconfdir}/aesm/
%__install -d %{buildroot}%{_libdir}/aesm/bundles
%__install -d %{buildroot}%{_datadir}/aesm/
%__install -d %{buildroot}%{_includedir}
%__install -d %{buildroot}%{_unitdir}

%__install -d %{buildroot}%{sgx_includedir}
%__install -d %{buildroot}%{sgx_libdir}

############################################################
#
# Bootstrap: the building blocks that can be compiled without
#            any significant external dependencies. The basic
#            runtime needed to build other libraries as enclaves
#
############################################################


# Host shared libs
for lib in capable ptrace
do
  cp build/linux/libsgx_${lib}.so %{buildroot}%{_libdir}/libsgx_${lib}.so
done

# Host static libs
for lib in capable
do
  cp build/linux/libsgx_${lib}.a %{buildroot}%{_libdir}/libsgx_${lib}.a
done
# XXX patch sgx-dcap to just look in libdir
cp %{buildroot}%{_libdir}/libsgx_capable.a \
   %{buildroot}%{sgx_libdir}/libsgx_capable.a

# SGX static libs
for lib in pthread tcxx tprotected_fs \
           tservice tstdc uprotected_fs uswitchless
do
  cp build/linux/libsgx_${lib}.a %{buildroot}%{sgx_libdir}/
done

# SGX headers
for edl in pthread tkey_exchange tprotected_fs tstdc tswitchless ttls
do
  cp common/inc/sgx_${edl}.edl %{buildroot}%{sgx_includedir}/
done

# Host tools
for bin in edger8r encrypt sign
do
  cp build/linux/sgx_${bin} %{buildroot}%{_bindir}/
done

cp -a common/inc/tlibc %{buildroot}%{sgx_includedir}/
cp -a common/inc/stdc++ %{buildroot}%{sgx_includedir}/
cp -a sdk/tlibcxx/include %{buildroot}%{sgx_includedir}/libcxx
cp common/inc/sgx*.{h,edl} %{buildroot}%{sgx_includedir}/

cp -a sdk/debugger_interface/linux/gdb-sgx-plugin %{buildroot}%{_datadir}/sgx-gdb-plugin
mv %{buildroot}%{_datadir}/sgx-gdb-plugin/sgx-gdb %{buildroot}%{_bindir}/

############################################################
#
# Full build: the pieces that depend on other libraries, most
#             especially needing crypto support
#
############################################################

%if %{sgx_bootstrap} == 0

# SGX static libs
for lib in trts tcrypto trts_sim tservice_sim \
           tkey_exchange ukey_exchange pcl pclsim tswitchless ttls utls
do
  cp build/linux/libsgx_${lib}.a %{buildroot}%{sgx_libdir}/
done

for lib in tls
do
  cp build/linux/libtdx_${lib}.a %{buildroot}%{sgx_libdir}/
done

# Native and SGX libs
for lib in epid launch quote_ex uae_service urts
do
  cp build/linux/libsgx_${lib}_sim.so %{buildroot}%{_libdir}/libsgx_${lib}_sim.so
  cp build/linux/libsgx_${lib}_deploy.so %{buildroot}%{_libdir}/libsgx_${lib}.so
done

for bin in config_cpusvn
do
  cp build/linux/sgx_${bin} %{buildroot}%{_bindir}/
done

%endif

%files -n sgx-enclave-devel
%license License.txt

%{_bindir}/sgx_edger8r
%{_bindir}/sgx_sign
%{_bindir}/sgx_encrypt
%{_bindir}/sgx-gdb

%if %{sgx_bootstrap} == 0

%{_bindir}/sgx_config_cpusvn

%endif

%dir %{_datadir}/sgx-gdb-plugin/
%{_datadir}/sgx-gdb-plugin/gdb_sgx_cmd
%{_datadir}/sgx-gdb-plugin/gdb_sgx_plugin.py
%{_datadir}/sgx-gdb-plugin/load_symbol_cmd.py
%{_datadir}/sgx-gdb-plugin/printers.py
%{_datadir}/sgx-gdb-plugin/readelf.py
%{_datadir}/sgx-gdb-plugin/sgx_emmt.py


%dir %{sgx_includedir}/

%{sgx_includedir}/libcxx/
%{sgx_includedir}/stdc++/
%{sgx_includedir}/tlibc/
%{sgx_includedir}/sgx*.h
%{sgx_includedir}/sgx_pthread.edl
%{sgx_includedir}/sgx_tkey_exchange.edl
%{sgx_includedir}/sgx_tprotected_fs.edl
%{sgx_includedir}/sgx_tstdc.edl
%{sgx_includedir}/sgx_tswitchless.edl
%{sgx_includedir}/sgx_ttls.edl

#{sgx_includedir}/ipp/
#{sgx_includedir}/mbedtls/
#{sgx_includedir}/tprotobuf/


%dir %{sgx_libdir}/

# XXX kill this copy of sgx_capable.a
%{sgx_libdir}/libsgx_capable.a
%{sgx_libdir}/libsgx_pthread.a
%{sgx_libdir}/libsgx_tcxx.a
%{sgx_libdir}/libsgx_tprotected_fs.a
%{sgx_libdir}/libsgx_tservice.a
%{sgx_libdir}/libsgx_tstdc.a
%{sgx_libdir}/libsgx_uprotected_fs.a
%{sgx_libdir}/libsgx_uswitchless.a

%{_libdir}/libsgx_capable.a
%{_libdir}/libsgx_capable.so
%{_libdir}/libsgx_ptrace.so


%if %{sgx_bootstrap} == 0
%{sgx_libdir}/libsgx_trts.a
%{sgx_libdir}/libsgx_tcrypto.a

%{_libdir}/libsgx_epid_sim.so
%{_libdir}/libsgx_launch_sim.so
%{_libdir}/libsgx_quote_ex_sim.so
%{_libdir}/libsgx_uae_service_sim.so
%{_libdir}/libsgx_urts_sim.so

%{sgx_libdir}/libsgx_pcl.a
%{sgx_libdir}/libsgx_pclsim.a
%{sgx_libdir}/libsgx_tkey_exchange.a
%{sgx_libdir}/libsgx_trts_sim.a
%{sgx_libdir}/libsgx_tservice_sim.a
%{sgx_libdir}/libsgx_tswitchless.a
%{sgx_libdir}/libsgx_ttls.a
%{sgx_libdir}/libsgx_ukey_exchange.a
%{sgx_libdir}/libsgx_utls.a
%{sgx_libdir}/libtdx_tls.a

# XXX fixme
#{_libdir}/pkgconfig/libsgx_epid_sim.pc
#{_libdir}/pkgconfig/libsgx_launch_sim.pc
#{_libdir}/pkgconfig/libsgx_quote_ex_sim.pc
#{_libdir}/pkgconfig/libsgx_uae_service_sim.pc
#{_libdir}/pkgconfig/libsgx_urts_sim.pc
%endif

%if %{sgx_bootstrap} == 0
%files -n sgx-platform-stubs-devel
%{_libdir}/libsgx_epid.so*
%{_libdir}/libsgx_launch.so*
%{_libdir}/libsgx_quote_ex.so*
%{_libdir}/libsgx_uae_service.so*
%{_libdir}/libsgx_urts.so*
%endif

%changelog
* Mon Jan 29 2024 Daniel P. Berrangé <berrange@redhat.com> - 2.22-1
- Initial packaging
