#!/bin/sh

set -e
set -v

spec=$1
srpmdir=$2
srcdir=$(dirname $spec)

if ! test -f $spec
then
    echo "missing $spec" && exit 1
fi

bootstrap=0
case $spec
in
    *:bootstrap/*)
	spec=$(echo $spec | sed -e 's/:bootstrap//')
	bootstrap=1
	;;
    *)
	;;
esac

speccopy=$(basename $spec)
cp $spec $speccopy
spec=$speccopy

if test $bootstrap == 1
then
    perl -i -p -e 's/define sgx_bootstrap 0/define sgx_bootstrap 1/' $spec
    perl -i -p -e 's/Release:.*/Release: 0bootstrap/' $spec
fi

for url in $(rpmspec -P ${spec} | grep Source | grep http | awk '{print $2}')
do
    tarball=$(basename ${url})
    if ! test -f ${srcdir}/${tarball}
    then
	(cd ${srcdir} && wget -O $tarball ${url})
    fi
done

rpmbuild -bs --define "_srcrpmdir ${srpmdir}" --define "_sourcedir ${srcdir}" ${spec}
rm ${spec}
