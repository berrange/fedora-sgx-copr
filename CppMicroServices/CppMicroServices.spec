Name: CppMicroServices
Version: 3.8.0
Release: 1%{?dist}
Summary: Build modular and dynamic service-oriented C++ applications
Url: http://cppmicroservices.org/
# XXX bundled licenses and audit this
License: Apache-2.0

Source0: https://github.com/CppMicroServices/CppMicroServices/archive/refs/tags/v3.8.0.tar.gz#/CppMicroServices-3.8.0.tar.gz

Patch1: 0001-Fully-use-system-boost.patch
Patch2: 0001-Use-system-jsoncpp-library.patch
Patch3: 0001-Use-system-linenoise-library.patch
Patch4: 0001-Use-system-civetweb-package.patch
Patch5: 0001-Use-system-spdlog-package.patch
Patch6: 0001-Removed-unused-absl-package.patch
Patch7: 0001-Replace-use-of-removed-htmlescape-function-from-sphi.patch
Patch8: 0001-Remove-broken-docs-python-monkeypatching.patch

Provides: bundled(rapidjson) = 091de040edb3355dcf2f4a18c425aec51b906f08
Provides: bundled(miniz) = 3.0.2
Provides: bundled(optionparser) = 1.7
Provides: bundled(tinyscheme) = 1.41

BuildRequires: gcc-c++
BuildRequires: doxygen
BuildRequires: cmake
BuildRequires: boost-devel
BuildRequires: civetweb-devel
BuildRequires: jsoncpp-devel
BuildRequires: rapidjson-devel
BuildRequires: linenoise-devel
BuildRequires: spdlog-devel
BuildRequires: python3-sphinx
BuildRequires: python3-sphinx_rtd_theme
BuildRequires: python3-breathe

%description
Design, build and manage complex applications with reusable components
and dynamic services. Extend and configure deployed systems at runtime.

%package devel
Summary: Development files for CppMicroServices

%description devel
Design, build and manage complex applications with reusable components
and dynamic services. Extend and configure deployed systems at runtime.

Development files for CppMicroServices

%prep
%autosetup -p1

# No longer used by code
rm -rf third_party/absl

# We're not building the benchmarks
rm -rf third_party/benchmark

# Patched to use Fedora package
rm -rf third_party/boost

# Patched to use Fedora package
rm -rf third_party/civetweb

# Not really third party code AFAICT
#rm -rf third_party/cppmicroservices_pe.h

# Not applicable to Linux build
rm -rf third_party/dirent_win32.h

# Patched to use Fedora pacakge
rm -rf third_party/googletest

# Patched to use Fedora package
rm -rf third_party/json
rm -rf third_party/jsoncpp.cpp

# Not actually used by the code
rm -rf third_party/libtelnet*

# Patched to use Fedora package
rm -rf third_party/linenoise*

# Has local modifications that are not in upstream
#rm -rf third_party/miniz*

# Not in Fedora and has local modifications that are not in upstream
#rm -rf third_party/optionparser.h

# XXX needs git master, Fedora has 1.1.0 and upstream
# has no intention of making new releases
# https://github.com/Tencent/rapidjson/issues/1006
#rm -rf third_party/rapidjson

# Patched to use Fedora package
rm -rf third_party/spdlog

# XXX add Fedora package
#rm -rf third_party/tinyscheme

# XXX tests don't successfully build
%build
%cmake \
  -DUS_USE_SYSTEM_BOOST:BOOL=ON \
  -DUS_SYSTEM_GTEST:BOOL=ON \
  -DUS_BUILD_TESTING:BOOL=OFF \
  -DUS_BUILD_DOC_HTML:BOOL=ON \
  -DUS_BUILD_DOC_MAN:BOOL=ON \
  -DLIBRARY_INSTALL_DIR=%{_libdir} \
  -DTOOLS_INSTALL_DIR=%{_bindir} \
  -DRUNTIME_INSTALL_DIR=%{_bindir} \
  -DARCHIVE_INSTALL_DIR=%{_libdir} \
  -DHEADER_INSTALL_DIR=%{_includedir}/cppmicroservices3 \
  -DAUXILIARY_INSTALL_DIR=%{_datadir}/cppmicroservices3 \
  -DDOC_INSTALL_DIR=%{_datadir}/doc/cppmicroservices3 \
  -DMAN_INSTALL_DIR=%{_mandir}
%cmake_build

%install
%cmake_install

%check
%ctest

%files
%{_libdir}/libConfigurationAdmind.so.*
%{_libdir}/libCppMicroServicesd.so.*
%{_libdir}/libDeclarativeServicesd.so.*
%{_libdir}/libLogServiced.so.*
# XXX no version
%{_libdir}/libusAsyncWorkServiced.so
#{_libdir}/libusAsyncWorkServiced.so.*
%{_libdir}/libusHttpServiced.so.*
# XXX no version
%{_libdir}/libusServiceComponentd.so
#{_libdir}/libusServiceComponentd.so.*
%{_libdir}/libusShellServiced.so.*
%{_libdir}/libusWebConsoled.so.*

%files devel
%{_datadir}/cppmicroservices3
%{_includedir}/cppmicroservices3
%{_bindir}/SCRCodeGen3
%{_bindir}/jsonschemavalidator
%{_bindir}/usResourceCompiler3
%{_bindir}/usShell3
%{_libdir}/libConfigurationAdmind.so
%{_libdir}/libCppMicroServicesd.so
%{_libdir}/libDeclarativeServicesd.so
%{_libdir}/libLogServiced.so
#{_libdir}/libusAsyncWorkServiced.so
%{_libdir}/libusHttpServiced.so
#%{_libdir}/libusServiceComponentd.so
%{_libdir}/libusShellServiced.so
%{_libdir}/libusWebConsoled.so
%{_datadir}/doc/cppmicroservices3/html
%{_mandir}/man1/usResourceCompiler3.1*
%{_mandir}/man1/usShell3.1*
%{_mandir}/man3/cppmicroservices-asyncworkservice.3*
%{_mandir}/man3/cppmicroservices-configadmin.3*
%{_mandir}/man3/cppmicroservices-ds.3*
%{_mandir}/man3/cppmicroservices-framework.3*
%{_mandir}/man3/cppmicroservices-httpservice.3*
%{_mandir}/man3/cppmicroservices-logservice.3*
%{_mandir}/man3/cppmicroservices-shellservice.3*
%{_mandir}/man3/cppmicroservices-webconsole.3*
%{_mandir}/man3/cppmicroservices.3*
%{_mandir}/man7/cppmicroservices-httpservice.7*
%{_mandir}/man7/cppmicroservices-shellservice.7*
%{_mandir}/man7/cppmicroservices-webconsole.7*
%{_mandir}/man7/cppmicroservices.7*
