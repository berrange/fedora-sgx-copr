
Name:           python-asn1
Version:        2.7.0
Release:        1%{?dist}
Summary:        A simple ASN.1 encoder and decoder
License:        BSD-2-Clause
Source0:        %{pypi_source asn1}
URL:            https://github.com/andrivet/python-asn1
BuildArch:      noarch

%description
Python-ASN1 is a simple ASN.1 encoder and decoder for Python 2.7 and 3.5+.

%package -n python3-asn1
Summary:    ASN.1 tools for Python 3
%{?python_provide:%python_provide python3-asn1}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pytest

%description -n python3-asn1
Python-ASN1 is a simple ASN.1 encoder and decoder for Python 2.7 and 3.5+.

%prep
%setup -n asn1-%{version} -q
sed -i -e "s/'enum-compat'//" setup.py
sed -i -e "s/enum-compat.*//" requirements.txt

%build
%py3_build

%install
%py3_install

%check
PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} -m pytest tests/

%files -n python3-asn1
%doc README.rst
%license LICENSE
%pycached %{python3_sitelib}/asn1.py
%{python3_sitelib}/asn1-%{version}-*.egg-info/

%changelog
* Tue Feb 27 2024 Daniel Berrange <berrange@mandriva.com> - 2.7.0
- Initial package

