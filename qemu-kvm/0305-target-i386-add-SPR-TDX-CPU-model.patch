From 46306b0319ddc0971fa35fb1e3465766e9b4e900 Mon Sep 17 00:00:00 2001
From: Chenyi Qiang <chenyi.qiang@intel.com>
Date: Fri, 30 Jul 2021 17:06:36 +0800
Subject: target/i386: add SPR-TDX CPU model

Add a new CPU model named SapphireRapids-TDX. It is a template of CPUID
for the TDX on SPR platform. Some CPU Info like FMS and model id is
retrived from a stepping E SPR SDP.

The listed features all belong to the TDX-supported CPUID bits, including:
* Native supported ones.
* TDX fixed1 features.
* Configurable (if native) && KVM-supported features; (If not supported
by KVM, exposing uncondionally may bring unexpected behavior).

In addition, the legacy KVM-specific default features are not fit for
TDX CPU model, so skip the assignment in x86_cpu_load_model.

Signed-off-by: Chenyi Qiang <chenyi.qiang@intel.com>
---
 target/i386/cpu.c         | 117 ++++++++++++++++++++++++++++++++++++++
 target/i386/kvm/kvm-cpu.c |   5 +-
 target/i386/kvm/tdx.h     |   1 +
 3 files changed, 122 insertions(+), 1 deletion(-)

diff --git a/target/i386/cpu.c b/target/i386/cpu.c
index a569a7c9bd..ba0cddb52a 100644
--- a/target/i386/cpu.c
+++ b/target/i386/cpu.c
@@ -3822,6 +3822,113 @@ static const X86CPUDefinition builtin_x86_defs[] = {
             { /* end of list */ },
         },
     },
+    {
+	.name = "TDX-SapphireRapids",
+	.level = 0x21,
+	.vendor = CPUID_VENDOR_INTEL,
+	.family = 6,
+	.model = 143,
+	.stepping = 4,
+	.features[FEAT_1_EDX] =
+	     /* CPUID_CONFIG: CPUID_ACPI CPUID_HT CPUID_TM CPUID_PBE */
+	    CPUID_FP87 | CPUID_VME | CPUID_DE | CPUID_PSE | CPUID_TSC |
+	    CPUID_MSR | CPUID_PAE | CPUID_MCE | CPUID_CX8 | CPUID_APIC |
+	    CPUID_SEP | CPUID_MTRR | CPUID_PGE | CPUID_MCA | CPUID_CMOV |
+	    CPUID_PAT | CPUID_PSE36 | CPUID_CLFLUSH | CPUID_DTS |
+	    CPUID_MMX | CPUID_FXSR | CPUID_SSE |
+	    CPUID_SSE2 | CPUID_SS,
+	.features[FEAT_1_ECX] =
+	    /*
+	     * CPUID_CONFIG: CPUID_EXT_EST CPUID_EXT_TM2 CPUID_EXT_XTPR
+	     * XFAM: CPUID_EXT_FMA CPUID_EXT_AVX CPUID_EXT_F16C
+	     */
+	    CPUID_EXT_SSE3 | CPUID_EXT_PCLMULQDQ | CPUID_EXT_DTES64 |
+	    CPUID_EXT_DSCPL | CPUID_EXT_SSSE3 | CPUID_EXT_FMA |
+	    CPUID_EXT_CX16 | CPUID_EXT_PDCM | CPUID_EXT_PCID |
+	    CPUID_EXT_SSE41 | CPUID_EXT_SSE42 | CPUID_EXT_X2APIC |
+	    CPUID_EXT_MOVBE | CPUID_EXT_POPCNT |
+	    CPUID_EXT_TSC_DEADLINE_TIMER | CPUID_EXT_AES |
+	    CPUID_EXT_XSAVE | CPUID_EXT_AVX | CPUID_EXT_F16C |
+	    CPUID_EXT_RDRAND,
+	.features[FEAT_8000_0001_EDX] =
+	    CPUID_EXT2_SYSCALL | CPUID_EXT2_NX | CPUID_EXT2_PDPE1GB |
+	    CPUID_EXT2_RDTSCP | CPUID_EXT2_LM,
+	.features[FEAT_8000_0001_ECX] =
+	    CPUID_EXT3_LAHF_LM | CPUID_EXT3_ABM | CPUID_EXT3_3DNOWPREFETCH,
+	.features[FEAT_7_0_EBX] =
+	    /*
+	     * CPUID_CONFIG: CPUID_7_0_EBX_BMI1 CPUID_7_0_EBX_BMI2
+	     *               CPUID_7_0_EBX_PQM CPUID_7_0_EBX_CQE
+         *               CPUID_7_0_EBX_ADX
+	     * XFAM: CPUID_7_0_EBX_AVX2 CPUID_7_0_EBX_AVX512F
+	     *       CPUID_7_0_EBX_AVX512DQ CPUID_7_0_EBX_AVX512IFMA
+	     *       CPUID_7_0_EBX_INTEL_PT CPUID_7_0_EBX_AVX512CD
+	     *       CPUID_7_0_EBX_AVX512BW CPUID_7_0_EBX_AVX512VL
+	     *
+	     *       CPUID_7_0_EBX_AVX512PF CPUID_7_0_EBX_AVX512ER not
+	     *       supported on SPR-D
+	     */
+	    CPUID_7_0_EBX_FSGSBASE | CPUID_7_0_EBX_BMI1 |
+	    CPUID_7_0_EBX_HLE | CPUID_7_0_EBX_AVX2 |
+	    CPUID_7_0_EBX_SMEP | CPUID_7_0_EBX_BMI2 |
+	    CPUID_7_0_EBX_ERMS | CPUID_7_0_EBX_INVPCID |
+	    CPUID_7_0_EBX_RTM | CPUID_7_0_EBX_AVX512F |
+	    CPUID_7_0_EBX_AVX512DQ | CPUID_7_0_EBX_RDSEED |
+	    CPUID_7_0_EBX_ADX | CPUID_7_0_EBX_SMAP |
+	    CPUID_7_0_EBX_AVX512IFMA | CPUID_7_0_EBX_CLFLUSHOPT |
+	    CPUID_7_0_EBX_CLWB | CPUID_7_0_EBX_AVX512CD |
+	    CPUID_7_0_EBX_SHA_NI | CPUID_7_0_EBX_AVX512BW |
+	    CPUID_7_0_EBX_AVX512VL,
+	.features[FEAT_7_0_ECX] =
+	    /*
+	     * CPUID_CONFIG: CPUID_7_0_ECX_WAITPKG CPUID_7_0_ECX_TME
+	     * XFAM: CPUID_7_0_ECX_AVX512_VBMI CPUID_7_0_ECX_PKU
+	     *        CPUID_7_0_ECX_AVX512_VBMI2 CPUID_7_0_ECX_CET_SHSTK
+	     *        CPUID_7_0_ECX_VAES CPUID_7_0_ECX_VPCLMULQDQ
+	     *        CPUID_7_0_ECX_AVX512VNNI CPUID_7_0_ECX_AVX512BITALG
+	     *        CPUID_7_0_ECX_AVX512_VPOPCNTDQ
+	     * ATTRIBUTE: CPUID_7_0_ECX_PKS
+	     */
+	    CPUID_7_0_ECX_AVX512_VBMI | CPUID_7_0_ECX_UMIP | CPUID_7_0_ECX_PKU |
+	    CPUID_7_0_ECX_WAITPKG | CPUID_7_0_ECX_AVX512_VBMI2 |
+	    CPUID_7_0_ECX_GFNI | CPUID_7_0_ECX_VAES |
+	    CPUID_7_0_ECX_VPCLMULQDQ | CPUID_7_0_ECX_AVX512VNNI |
+	    CPUID_7_0_ECX_AVX512BITALG | CPUID_7_0_ECX_AVX512_VPOPCNTDQ |
+	    CPUID_7_0_ECX_LA57 | CPUID_7_0_ECX_RDPID |
+	    CPUID_7_0_ECX_BUS_LOCK_DETECT | CPUID_7_0_ECX_CLDEMOTE |
+	    CPUID_7_0_ECX_MOVDIRI | CPUID_7_0_ECX_MOVDIR64B,
+	.features[FEAT_7_0_EDX] =
+	     /*
+	     * XFAM: CPUID_7_0_EDX_ULI CPUID_7_0_EDX_ARCH_LBR
+	     *       CPUID_7_0_EDX_AVX512_VP2INTERSECT
+	     *       CPUID_7_0_EDX_AMX_BF16 CPUID_7_0_EDX_CET_IBT
+	     *       CPUID_7_0_EDX_AMX_INT8 CPUID_7_0_EDX_AMX_TILE
+	     *
+	     *
+	     * CPUID_7_0_EDX_AVX512_4VNNIW  CPUID_7_0_EDX_AVX512_4FMAPS
+	     * not supported
+	     */
+	    CPUID_7_0_EDX_FSRM | CPUID_7_0_EDX_SERIALIZE |
+	    CPUID_7_0_EDX_TSX_LDTRK | CPUID_7_0_EDX_AMX_BF16 |
+	    CPUID_7_0_EDX_AVX512_FP16 | CPUID_7_0_EDX_AMX_TILE |
+	    CPUID_7_0_EDX_AMX_INT8  | CPUID_7_0_EDX_SPEC_CTRL |
+	    CPUID_7_0_EDX_STIBP | CPUID_7_0_EDX_ARCH_CAPABILITIES |
+	    CPUID_7_0_EDX_CORE_CAPABILITY | CPUID_7_0_EDX_SPEC_CTRL_SSBD,
+	.features[FEAT_XSAVE] =
+	    /*
+	     * XFAM: CPUID_XSAVE_XFD depend on all of XFAM[]
+	     */
+	    CPUID_XSAVE_XSAVEOPT | CPUID_XSAVE_XSAVEC |
+	    CPUID_XSAVE_XGETBV1 | CPUID_XSAVE_XSAVES,
+	.features[FEAT_6_EAX] =
+	    CPUID_6_EAX_ARAT,
+	.features[FEAT_KVM] = (1ULL << KVM_FEATURE_NOP_IO_DELAY) |
+	    (1ULL << KVM_FEATURE_PV_UNHALT) | (1ULL << KVM_FEATURE_PV_TLB_FLUSH) |
+	    (1ULL << KVM_FEATURE_PV_SEND_IPI) | (1ULL << KVM_FEATURE_POLL_CONTROL) |
+	    (1ULL << KVM_FEATURE_PV_SCHED_YIELD) | (1ULL << KVM_FEATURE_MSI_EXT_DEST_ID),
+	.xlevel = 0x80000008,
+	.model_id = "Genuine Intel(R) CPU",
+    },
     {
         .name = "Denverton",
         .level = 21,
@@ -7084,6 +7191,16 @@ static void x86_cpu_realizefn(DeviceState *dev, Error **errp)
         if (cpu->phys_bits == 0) {
             cpu->phys_bits = TCG_PHYS_ADDR_BITS;
         }
+#ifndef CONFIG_USER_ONLY
+        if (is_tdx_vm()) {
+            /*
+             * Some CPUID bits can't be reflected in env->features[],
+             * but they still have restrictions in TDX VM.
+             * e.g. physical address bits is fixed to 0x34.
+             */
+            cpu->phys_bits = TDX_PHYS_ADDR_BITS;
+        }
+#endif
     } else {
         /* For 32 bit systems don't use the user set value, but keep
          * phys_bits consistent with what we tell the guest.
diff --git a/target/i386/kvm/kvm-cpu.c b/target/i386/kvm/kvm-cpu.c
index 85d54be4df..da6bb077ca 100644
--- a/target/i386/kvm/kvm-cpu.c
+++ b/target/i386/kvm/kvm-cpu.c
@@ -14,6 +14,7 @@
 #include "qapi/error.h"
 #include "sysemu/sysemu.h"
 #include "hw/boards.h"
+#include "tdx.h"
 
 #include "tdx.h"
 #include "kvm_i386.h"
@@ -182,7 +183,9 @@ static void kvm_cpu_instance_init(CPUState *cs)
         }
 
         /* Special cases not set in the X86CPUDefinition structs: */
-        x86_cpu_apply_props(cpu, kvm_default_props);
+        if (!is_tdx_vm()) {
+            x86_cpu_apply_props(cpu, kvm_default_props);
+        }
     }
 
     if (cpu->max_features) {
diff --git a/target/i386/kvm/tdx.h b/target/i386/kvm/tdx.h
index 0830fd460c..99d482e49f 100644
--- a/target/i386/kvm/tdx.h
+++ b/target/i386/kvm/tdx.h
@@ -13,6 +13,7 @@
 
 #define TYPE_TDX_GUEST "tdx-guest"
 #define TDX_GUEST(obj)  OBJECT_CHECK(TdxGuest, (obj), TYPE_TDX_GUEST)
+#define TDX_PHYS_ADDR_BITS  52
 
 typedef struct TdxGuestClass {
     ConfidentialGuestSupportClass parent_class;
-- 
2.41.0

