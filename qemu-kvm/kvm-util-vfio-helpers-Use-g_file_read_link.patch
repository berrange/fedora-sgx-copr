From 7316ddf6a13e8672c8d47e1f60a0cefc036b28b7 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?C=C3=A9dric=20Le=20Goater?= <clg@redhat.com>
Date: Wed, 12 Jul 2023 17:46:57 +0200
Subject: util/vfio-helpers: Use g_file_read_link()
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

RH-Author: Cédric Le Goater <clg@redhat.com>
RH-MergeRequest: 179: vfio: live migration support
RH-Bugzilla: 2192818
RH-Acked-by: Eric Auger <eric.auger@redhat.com>
RH-Acked-by: Miroslav Rezanina <mrezanin@redhat.com>
RH-Commit: [5/28] 3545a07c967782dba8dd081415232f91d3f600a9 (clegoate/qemu-kvm-c9s)

Bugzilla: https://bugzilla.redhat.com/2192818

commit dbdea0dbfe2c
Author: Akihiko Odaki <akihiko.odaki@daynix.com>
Date:   Tue May 23 11:39:12 2023 +0900

    util/vfio-helpers: Use g_file_read_link()

    When _FORTIFY_SOURCE=2, glibc version is 2.35, and GCC version is
    12.1.0, the compiler complains as follows:

    In file included from /usr/include/features.h:490,
                     from /usr/include/bits/libc-header-start.h:33,
                     from /usr/include/stdint.h:26,
                     from /usr/lib/gcc/aarch64-unknown-linux-gnu/12.1.0/include/stdint.h:9,
                     from /home/alarm/q/var/qemu/include/qemu/osdep.h:94,
                     from ../util/vfio-helpers.c:13:
    In function 'readlink',
        inlined from 'sysfs_find_group_file' at ../util/vfio-helpers.c:116:9,
        inlined from 'qemu_vfio_init_pci' at ../util/vfio-helpers.c:326:18,
        inlined from 'qemu_vfio_open_pci' at ../util/vfio-helpers.c:517:9:
    /usr/include/bits/unistd.h:119:10: error: argument 2 is null but the corresponding size argument 3 value is 4095 [-Werror=nonnull]
      119 |   return __glibc_fortify (readlink, __len, sizeof (char),
          |          ^~~~~~~~~~~~~~~

    This error implies the allocated buffer can be NULL. Use
    g_file_read_link(), which allocates buffer automatically to avoid the
    error.

    Signed-off-by: Akihiko Odaki <akihiko.odaki@daynix.com>
    Reviewed-by: Philippe Mathieu-Daudé <philmd@linaro.org>
    Reviewed-by: Cédric Le Goater <clg@redhat.com>
    Signed-off-by: Cédric Le Goater <clg@redhat.com>

Signed-off-by: Cédric Le Goater <clg@redhat.com>

Patch-name: kvm-util-vfio-helpers-Use-g_file_read_link.patch
Patch-id: 117
Patch-present-in-specfile: True
---
 util/vfio-helpers.c | 8 +++++---
 1 file changed, 5 insertions(+), 3 deletions(-)

diff --git a/util/vfio-helpers.c b/util/vfio-helpers.c
index 2d8af38f88..f8bab46c68 100644
--- a/util/vfio-helpers.c
+++ b/util/vfio-helpers.c
@@ -106,15 +106,17 @@ struct QEMUVFIOState {
  */
 static char *sysfs_find_group_file(const char *device, Error **errp)
 {
+    g_autoptr(GError) gerr = NULL;
     char *sysfs_link;
     char *sysfs_group;
     char *p;
     char *path = NULL;
 
     sysfs_link = g_strdup_printf("/sys/bus/pci/devices/%s/iommu_group", device);
-    sysfs_group = g_malloc0(PATH_MAX);
-    if (readlink(sysfs_link, sysfs_group, PATH_MAX - 1) == -1) {
-        error_setg_errno(errp, errno, "Failed to find iommu group sysfs path");
+    sysfs_group = g_file_read_link(sysfs_link, &gerr);
+    if (gerr) {
+        error_setg(errp, "Failed to find iommu group sysfs path: %s",
+                   gerr->message);
         goto out;
     }
     p = strrchr(sysfs_group, '/');
-- 
2.41.0

