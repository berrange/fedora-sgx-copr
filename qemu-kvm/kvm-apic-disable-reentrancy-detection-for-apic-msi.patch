From 87225c32815f2fe80904bb124ed5fa58763f5fc5 Mon Sep 17 00:00:00 2001
From: Jon Maloy <jmaloy@redhat.com>
Date: Tue, 9 May 2023 10:29:03 -0400
Subject: apic: disable reentrancy detection for apic-msi

RH-Author: Jon Maloy <jmaloy@redhat.com>
RH-MergeRequest: 165: memory: prevent dma-reentracy issues
RH-Jira: RHEL-516
RH-Acked-by: Miroslav Rezanina <mrezanin@redhat.com>
RH-Commit: [8/13] 329f3b1c02fc42d85c821dd14c70e6b885cf849a (jmaloy/jmaloy-qemu-kvm-2)

Jira: https://issues.redhat.com/browse/RHEL-516
Upstream: Merged
CVE: CVE-2023-2680

commit 50795ee051a342c681a9b45671c552fbd6274db8
Author: Alexander Bulekov <alxndr@bu.edu>
Date:   Thu Apr 27 17:10:13 2023 -0400

    apic: disable reentrancy detection for apic-msi

    As the code is designed for re-entrant calls to apic-msi, mark apic-msi
    as reentrancy-safe.

    Signed-off-by: Alexander Bulekov <alxndr@bu.edu>
    Reviewed-by: Darren Kenny <darren.kenny@oracle.com>
    Message-Id: <20230427211013.2994127-9-alxndr@bu.edu>
    Signed-off-by: Thomas Huth <thuth@redhat.com>

Signed-off-by: Jon Maloy <jmaloy@redhat.com>

Patch-name: kvm-apic-disable-reentrancy-detection-for-apic-msi.patch
Patch-id: 91
Patch-present-in-specfile: True
---
 hw/intc/apic.c | 7 +++++++
 1 file changed, 7 insertions(+)

diff --git a/hw/intc/apic.c b/hw/intc/apic.c
index 20b5a94073..ac3d47d231 100644
--- a/hw/intc/apic.c
+++ b/hw/intc/apic.c
@@ -885,6 +885,13 @@ static void apic_realize(DeviceState *dev, Error **errp)
     memory_region_init_io(&s->io_memory, OBJECT(s), &apic_io_ops, s, "apic-msi",
                           APIC_SPACE_SIZE);
 
+    /*
+     * apic-msi's apic_mem_write can call into ioapic_eoi_broadcast, which can
+     * write back to apic-msi. As such mark the apic-msi region re-entrancy
+     * safe.
+     */
+    s->io_memory.disable_reentrancy_guard = true;
+
     s->timer = timer_new_ns(QEMU_CLOCK_VIRTUAL, apic_timer, s);
     local_apics[s->id] = s;
 
-- 
2.41.0

