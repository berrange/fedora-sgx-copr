# This package only provides SGX enclave content
%global debug_package %{nil}

# We need direct control over build flags used for the
# SGX enclave code.
%undefine _auto_set_build_flags

Name:		sgx-rapidjson
Version:	1.1.0
Release:	1%{?dist}
Summary:	Fast JSON parser and generator for C++

# Most files are MIT, rapidjson/msinttypes/{stdint,inttypes}.h are BSD
License:	MIT AND BSD-3-Clause
URL:		http://rapidjson.org/
Source0:	https://github.com/Tencent/rapidjson/archive/v%{version}/rapidjson-%{version}.tar.gz
# https://github.com/Tencent/rapidjson/pull/719
Patch:          0001-Removed-non-compiling-assignment-operator.-Fixed-718.patch
# https://github.com/Tencent/rapidjson/pull/719
Patch:          0002-Explicitly-disable-copy-assignment-operator.patch
# https://github.com/Tencent/rapidjson/pull/1137
Patch:          0003-Three-way-comparison-for-CLang-10-fix-1679.patch
# https://github.com/Tencent/rapidjson/pull/1679
Patch:          0004-Fix-recursive-operator-call-in-C-20-1846.patch
# https://github.com/Tencent/rapidjson/pull/1847
Patch:          0005-gate-definition-of-symmetric-equality-operators-on-i.patch
# https://github.com/Tencent/rapidjson/pull/2091
Patch:          0006-do-not-define-operator-in-C-20.patch
# Downstream-patch for gtest
Patch:          0007-do-not-include-gtest_src_dir.patch

BuildRequires:  sgx-srpm-macros
BuildRequires:	cmake
BuildRequires:	make
BuildRequires:	gcc-c++

%description
RapidJSON is a fast JSON parser and generator for C++.  It was		
inspired by RapidXml.							
									
  RapidJSON is small but complete.  It supports both SAX and DOM style	
  API. The SAX parser is only a half thousand lines of code.		
									
  RapidJSON is fast.  Its performance can be comparable to strlen().	
  It also optionally supports SSE2/SSE4.1 for acceleration.		
									
  RapidJSON is self-contained.  It does not depend on external		
  libraries such as BOOST.  It even does not depend on STL.		
									
  RapidJSON is memory friendly.  Each JSON value occupies exactly	
  16/20 bytes for most 32/64-bit machines (excluding text string).  By	
  default it uses a fast memory allocator, and the parser allocates	
  memory compactly during parsing.					
									
  RapidJSON is Unicode friendly.  It supports UTF-8, UTF-16, UTF-32	
  (LE & BE), and their detection, validation and transcoding		
  internally.  For example, you can read a UTF-8 file and let RapidJSON	
  transcode the JSON strings into UTF-16 in the DOM.  It also supports	
  surrogates and "\u0000" (null character).				
									
JSON(JavaScript Object Notation) is a light-weight data exchange	
format.  RapidJSON should be in fully compliance with RFC4627/ECMA-404.


%package -n sgx-enclave-rapidjson-devel
Summary: SGX development header files for rapidjson

%description -n sgx-enclave-rapidjson-devel
SGX development header files for rapidjson

%prep
%autosetup -p 1 -n rapidjson-%{version}

# Remove bundled code
rm -rf thirdparty

# Convert DOS line endings to unix
for file in "license.txt" $(find example -type f -name *.c*)
do
  sed -e "s/\r$//g" < ${file} > ${file}.new && \
    touch -r ${file} ${file}.new && \
    mv -f ${file}.new ${file}
done

# Remove -march=native and -Werror from compile commands
find . -type f -name CMakeLists.txt -print0 | \
  xargs -0r sed -i -e "s/-march=native/ /g" -e "s/-Werror//g"


%build
%sgx_cmake \
    -DRAPIDJSON_BUILD_CXX11:BOOL=OFF \
    -DRAPIDJSON_BUILD_DOC:BOOL=OFF \
    -DRAPIDJSON_BUILD_EXAMPLES:BOOL=OFF \
    -DRAPIDJSON_BUILD_TESTS:BOOL=OFF
%sgx_cmake_build

%install
%sgx_cmake_install
rm -rf %{buildroot}%{sgx_prefix}/share/doc

%files -n sgx-enclave-rapidjson-devel
%license license.txt
%{sgx_libdir}/cmake/RapidJSON/
%{sgx_libdir}/pkgconfig/RapidJSON.pc
%{sgx_includedir}/rapidjson/

%changelog
* Wed Feb 7  2024 Daniel P. Berrangé <berrange@redhat.com> - 1.1.0-1
- Initial packaging, derived from native spec
