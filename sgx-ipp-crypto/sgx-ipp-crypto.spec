# This package only provides SGX enclave content
%define debug_package %{nil}

# We need direct control over build flags used for the
# SGX enclave code.
%undefine _auto_set_build_flags

Name: sgx-ipp-crypto
Version: 2021.7
Release: 1%{?dist}
Summary: SGX support for IPP Cryptography

License: Apache-2.0
URL: https://github.com/intel/ipp-crypto
Source0: %{url}/archive/ippcp_%{version}/ippcp_%{version}.tar.gz
# From linux-sgx.git/external/ippcp_internal/inc/sgx_ippcp.h
Source1: sgx_ippcp.h
# From linux-sgx.git/build-scripts/sgx-asm-pp.py
Source2: sgx-asm-pp.py

Patch1: ippcp-werror.patch

# From linux-sgx.git/external/ippcp_external/inc/
Patch2: ippcp21u7.patch

BuildRequires: cmake
BuildRequires: make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: nasm

BuildRequires: sgx-srpm-macros
BuildRequires: sgx-enclave-devel

%description
SGX support for IPP Cryptography

%package -n sgx-enclave-ipp-crypto-devel
Summary: SGX support for IPP Cryptography

%description -n sgx-enclave-ipp-crypto-devel
SGX support for IPP Cryptography

This package provides the development libraries and header files

%prep
%setup -n ipp-crypto-ippcp_%{version}
%patch -P 1 -p1

%build

ippcflags=$(echo %{sgx_enclave_cflags} | sed -e 's/-nostdinc//' -e 's/-fpie/-fpic/')

# XXX this nasm override doesn't appear to be honoured
ASM_NASM="%{python3} %{SOURCE2} --assembler=nasm --MITIGATION-CVE-2020-0551=%{sgx_cve_2020_0551_mitigation}" \
%sgx_cmake  \
    -DCMAKE_VERBOSE_MAKEFILE=on \
    -DCMAKE_C_COMPILER_WORKS=TRUE \
    -DCMAKE_CXX_COMPILER_WORKS=TRUE \
    -DARCH=intel64 \
    -DNO_CRYPTO_MB=TRUE \
    -DCMAKE_C_FLAGS="$ippcflags" \
    -DCMAKE_CXX_FLAGS="%{sgx_enclave_cxxflags}"

%make_build -C %{sgx_triplet} ippcp_s

%install
%__install -d %{buildroot}%{sgx_libdir}
%__install -d %{buildroot}%{sgx_includedir}/ipp/

cp %{sgx_triplet}/.build/RELEASE/lib/libippcp.a %{buildroot}%{sgx_libdir}/libsgx_ippcp.a

patch include/ippcp.h < %{PATCH2}
cp include/*.h %{buildroot}%{sgx_includedir}/ipp/
cp %{SOURCE1}  %{buildroot}%{sgx_includedir}/ipp/

%files -n sgx-enclave-ipp-crypto-devel
%{sgx_libdir}/libsgx_ippcp.a
%{sgx_includedir}/ipp/
