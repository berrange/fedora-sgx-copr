
# We need direct control over build flags used for the
# SGX enclave code.
#
# Unfortunately this package also builds some native
# code, but he makefiles for native & enclave code
# are so horribly tangled it is difficult to make
# native code honour default RPM CFLAGS while keeping
# those flags out of enclave code
%undefine _auto_set_build_flags

%global __provides_exclude ^lib(ipc|oal|utils|urts_internal|(ecdsa_quote|epid_quote|le_launch|linux_network|pce|quote_ex)_service_bundle)\.so.*$
%global __requires_exclude ^lib(ipc|oal|utils|urts_internal)\.so.*$

%global dcap_version 1.19

Name: sgx-psw
Version: 2.22
Release: 1%{?dist}
Summary: SGX Platform Software

License: BSD-3-Clause AND (BSD-3-Clause OR GPL-2.0)
URL: https://github.com/intel/linux-sgx

Source0: https://github.com/intel/linux-sgx/archive/refs/tags/sgx_%{version}.tar.gz#/linux-sgx-sgx_%{version}.tar.gz

Source1: https://download.01.org/intel-sgx/sgx-linux/%{version}/prebuilt_ae_%{version}.tar.gz

Source2: aesmd.sysusers.conf
Source3: aesmd.service

Source4: sgxprv.sysusers.conf
Source5: 92-sgx-provision.rules

# https://github.com/Intel-EPID-SDK/epid-sdk
#
# Upstream is discontinued/archived.
# Last upstream epid-sdk wass 8.0.0
#
# The bundled code is derived from 6.0.0, with
# various downstream changes especially to the
# build system (likely to make it repreoducible)
#
# 8.0.0 had some API changes so it is not clear
# it would be compatible with what SGX neesds
#
#  => continuing to bundle is only practical option
Provides: bundled(epid-sdk) == 6.0.0

# http://software.intel.com/sites/default/files/article/185457/librdrand-windows-r1.zip
#
# There is no upstream project, just the zip file dump.
# There is no shared library, so any usage would be
# static linked regardless.
#
#  => no benefit in unbundling
Provides: bundled(librdrand)

Patch1: 0001-Use-distro-CppMicroServices-instead-of-bundled-copy.patch
Patch2: 0002-Use-distro-libcrypto-instad-of-pre-built-bundled-cop.patch
Patch3: 0003-Use-distro-provided-SGX-enclave-libraries-instad-of-.patch
Patch4: 0004-Fix-path-to-find-sgx_edger8r-binary.patch
Patch5: 0005-Fix-compat-with-GCC-14.patch
Patch6: 0006-Fix-epid-build-compat-with-GCC-14.patch

BuildRequires: sgx-srpm-macros
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: cmake
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool

BuildRequires: openssl-devel
BuildRequires: protobuf-compiler
BuildRequires: protobuf-devel

BuildRequires: CppMicroServices-devel
BuildRequires: sgx-enclave-devel = %{version}
BuildRequires: sgx-enclave-dcap-devel = %{dcap_version}
BuildRequires: sgx-platform-dcap-libs = %{dcap_version}
BuildRequires: sgx-platform-dcap-devel = %{dcap_version}
BuildRequires: curl-devel
BuildRequires: systemd-rpm-macros
BuildRequires: perl-interpreter

# SGX is a feature supported and verified on x86_64 only.
ExclusiveArch: x86_64

%description
The Intel SGX SDK is a collection of APIs, libraries, documentations and
tools that allow software developers to create and debug Intel SGX 
enabled applications in C/C++.


%package -n sgx-enclave-unsigned
Summary: SGX enclaves unsigned

Provides: sgx-enclave(le:unsigned) = %{version}
Provides: sgx-enclave(pce:unsigned) = %{version}
Provides: sgx-enclave(pve:unsigned) = %{version}
Provides: sgx-enclave(qe:unsigned) = %{version}

%description -n sgx-enclave-unsigned
This package contains the signed SGX enclave libraries.


%package -n sgx-enclave-signed
Summary: SGX enclaves unsigned

Provides: sgx-enclave(le:signed:intel) = %{version}
Provides: sgx-enclave(pce:signed:intel) = %{version}
Provides: sgx-enclave(pve:signed:intel) = %{version}
Provides: sgx-enclave(qe:signed:intel) = %{version}

%description -n sgx-enclave-signed
This package contains the signed SGX enclave libraries.


%package -n sgx-platform-devel
Summary: SGX platform libraries development

%description -n sgx-platform-devel
This package contains the header files, libraries and tools required
to build applications that interact with SGX enclaves on the platform.


%package -n sgx-platform-libs
Summary: SGX platform libraries runtime

%description -n sgx-platform-libs
This package contains the runtime libraries and tools required
to run applications that interact with SGX enclaves on the platform.


%package -n sgx-platform-aesm
Summary: SGX platform Architectural Enclave Service Manager
# XXX ELF dep isn't auto-picked up since the binary isn't
# yet put in /usr/bin, so we need this temporary manual dep
Requires: CppMicroServices

# Core SGX enclaves
Suggests: sgx-enclave(le:signed:intel) >= %{version}
Suggests: sgx-enclave(pce:signed:intel) >= %{version}
Suggests: sgx-enclave(pve:signed:intel) >= %{version}
Suggests: sgx-enclave(qe:signed:intel) >= %{version}

# Additional DCAP enclaves
Suggests: sgx-enclave(qe3:signed:intel) >= %{dcap_version}
Suggests: sgx-enclave(qve:signed:intel) >= %{dcap_version}
Suggests: sgx-enclave(id_enclave:signed:intel) >= %{dcap_version}

# Additional DCAP TDX VM enclave
Suggests: sgx-enclave(tdqe:signed:intel) >= %{version}

%description -n sgx-platform-aesm
This package contains the  Architectural Enclave Service Manager
(AESM) daemon.


%prep
%autosetup -n linux-sgx-sgx_%{version} -p1

# Temporarily preserve pieces we don't un-bundle
mv external/epid-sdk external/rdrand external/vtune .

# Purge everything else to avoid accidentally building
# bundled code
rm -rf external/*
rm -rf sdk/gperftools/

# Restore the pieces we keep bundled
mv epid-sdk rdrand vtune external/

# The pre-built and (critically) signed enclaves
tar zxvf %{SOURCE1}

%build

%make_build -C psw -j1 \
    SGX_SDK=%{sgx_prefix} \
    CPPMICROSERVICES_CMAKE=%{_datadir}/cppmicroservices3/cmake/

for enclave in le pce pve qe
do
  %make_build -C psw/ae/$enclave \
    SGX_BIN_DIR=%{_bindir} \
    SGX_SDK=%{sgx_prefix} \
    SGX_LIB_DIR=%{sgx_libdir} \
    $enclave.so
done

%install

%__install -d %{buildroot}%{_bindir}
%__install -d %{buildroot}%{_sbindir}
%__install -d %{buildroot}%{_sysconfdir}/aesmd/
%__install -d %{buildroot}%{_libdir}/aesmd/bundles
%__install -d %{buildroot}%{_datadir}/aesmd/
%__install -d %{buildroot}%{_includedir}
%__install -d %{buildroot}%{_unitdir}
%__install -d %{buildroot}%{_sysusersdir}
%__install -d %{buildroot}%{_udevrulesdir}

%__install -d %{buildroot}%{sgx_includedir}
%__install -d %{buildroot}%{sgx_libdir}


for i in sgx_enclave_common sgx_epid sgx_launch sgx_quote_ex
do
  cp build/linux/lib${i}.so \
    %{buildroot}%{_libdir}/lib${i}.so.1
  ln -s lib${i}.so.1 %{buildroot}%{_libdir}/lib${i}.so
done

cp build/linux/libsgx_urts.so \
  %{buildroot}%{_libdir}/libsgx_urts.so.2
ln -s libsgx_urts.so.2 %{buildroot}%{_libdir}/libsgx_urts.so

# XXX not versioned for some reason
cp build/linux/libsgx_uae_service.so \
  %{buildroot}%{_libdir}/libsgx_uae_service.so

cp psw/enclave_common/sgx_enclave_common.h \
   %{buildroot}%{_includedir}/sgx_enclave_common.h

cp build/linux/aesm_service \
   %{buildroot}%{_libdir}/aesmd/aesmd

# XXX it looks for files relative to its bniary, so we
# need this wrapper. Patch the source and kill this
cat >> %{buildroot}%{_sbindir}/aesmd <<EOF
#!/bin/sh

export LD_LIBRARY_PATH=%{_libdir}/aesmd/
exec %{_libdir}/aesmd/aesmd "\$@"
EOF
chmod +x %{buildroot}%{_sbindir}/aesmd

# XXX We want the config in /etc, but it only looks
# in the same dir as its binary, so until wea
# patch the source symlink hack will have todo.
cp psw/ae/aesm_service/config/network/aesmd.conf \
   %{buildroot}%{_sysconfdir}/aesmd.conf
ln -s ../../..%{_sysconfdir}/aesmd.conf \
   %{buildroot}%{_libdir}/aesmd/aesmd.conf

cp build/linux/libipc.so \
   %{buildroot}%{_libdir}/aesmd/libipc.so
cp build/linux/liboal.so \
   %{buildroot}%{_libdir}/aesmd/liboal.so
cp build/linux/libutils.so \
   %{buildroot}%{_libdir}/aesmd/libutils.so
# XXX can it just use liburts.so ?
cp build/linux/liburts_internal.so \
   %{buildroot}%{_libdir}/aesmd/liburts_internal.so

cp build/linux/le_prod_css.bin \
   %{buildroot}%{_datadir}/aesmd/le_prod_css.bin
cp build/linux/white_list_cert_to_be_verify.bin \
   %{buildroot}%{_datadir}/aesmd/white_list_cert_to_be_verify.bin

ln -s ../../..%{_datadir}/aesmd/le_prod_css.bin \
   %{buildroot}%{_libdir}/aesmd/le_prod_css.bin
ln -s ../../..%{_datadir}/aesmd/white_list_cert_to_be_verify.bin \
   %{buildroot}%{_libdir}/aesmd/white_list_cert_to_be_verify.bin

for enclave in pce qe pve le
do
  cp psw/ae/${enclave}/${enclave}.so \
    %{buildroot}%{sgx_libdir}/libsgx_${enclave}.so

  cp build/linux/libsgx_${enclave}.signed.so \
    %{buildroot}%{sgx_libdir}/libsgx_${enclave}.signed.so.1
done

for i in pce linux_network ecdsa_quote quote_ex epid_quote le_launch
do
  cp build/linux/bundles/lib${i}_service_bundle.so \
    %{buildroot}%{_libdir}/aesmd/bundles/lib${i}_service_bundle.so
done

cp build/linux/aesmd.service \
    %{buildroot}%{_unitdir}/aesmd.service

# Home dir for 'aesmd' user
%__install -d %{buildroot}%{_sharedstatedir}/aesmd
%__install -d %{buildroot}%{_rundir}/aesmd

%__install %{SOURCE2} %{buildroot}%{_sysusersdir}/aesmd.conf
%__install %{SOURCE3} %{buildroot}%{_unitdir}/aesmd.service


%__install %{SOURCE4} %{buildroot}%{_sysusersdir}/sgxprv.conf
%__install %{SOURCE5} %{buildroot}%{_udevrulesdir}/92-sgx-provision.rules

%pre -n sgx-platform-aesm
%sysusers_create_compat %{SOURCE2}

%post -n sgx-platform-aesm
%systemd_post aesmd.service

%preun -n sgx-platform-aesm
%systemd_preun aesmd.service

%postun -n sgx-platform-aesm
%systemd_postun_with_restart aesmd.service


%pre -n sgx-platform-libs
%sysusers_create_compat %{SOURCE4}


%files -n sgx-enclave-unsigned
%{sgx_libdir}/libsgx_le.so
%{sgx_libdir}/libsgx_pce.so
%{sgx_libdir}/libsgx_pve.so
%{sgx_libdir}/libsgx_qe.so

%files -n sgx-enclave-signed
%{sgx_libdir}/libsgx_le.signed.so.1
%{sgx_libdir}/libsgx_pce.signed.so.1
%{sgx_libdir}/libsgx_pve.signed.so.1
%{sgx_libdir}/libsgx_qe.signed.so.1

%files -n sgx-platform-devel
%{_includedir}/sgx_enclave_common.h
%{_libdir}/libsgx_enclave_common.so
%{_libdir}/libsgx_epid.so
%{_libdir}/libsgx_launch.so
%{_libdir}/libsgx_quote_ex.so
# It isn't versioned yet for some reason
#{_libdir}/libsgx_uae_service.so
%{_libdir}/libsgx_urts.so

%files -n sgx-platform-libs
%{_libdir}/libsgx_enclave_common.so.1
%{_libdir}/libsgx_epid.so.1
%{_libdir}/libsgx_launch.so.1
%{_libdir}/libsgx_quote_ex.so.1
%{_libdir}/libsgx_uae_service.so
%{_libdir}/libsgx_urts.so.2
%{_sysusersdir}/sgxprv.conf
%{_udevrulesdir}/92-sgx-provision.rules

%files -n sgx-platform-aesm
%{_sbindir}/aesmd
%{_unitdir}/aesmd.service
%{_sysconfdir}/aesmd.conf
%{_libdir}/aesmd/aesmd
%{_libdir}/aesmd/bundles/libecdsa_quote_service_bundle.so
%{_libdir}/aesmd/bundles/libepid_quote_service_bundle.so
%{_libdir}/aesmd/bundles/lible_launch_service_bundle.so
%{_libdir}/aesmd/bundles/liblinux_network_service_bundle.so
%{_libdir}/aesmd/bundles/libpce_service_bundle.so
%{_libdir}/aesmd/bundles/libquote_ex_service_bundle.so
%{_libdir}/aesmd/white_list_cert_to_be_verify.bin
%{_libdir}/aesmd/le_prod_css.bin
%{_libdir}/aesmd/aesmd.conf
%{_libdir}/aesmd/liboal.so
%{_libdir}/aesmd/libipc.so
%{_libdir}/aesmd/libutils.so
%{_libdir}/aesmd/liburts_internal.so
%{_datadir}/aesmd/white_list_cert_to_be_verify.bin
%{_datadir}/aesmd/le_prod_css.bin
%attr(0700,aesmd,aesmd) %{_sharedstatedir}/aesmd
%{_sysusersdir}/aesmd.conf
%attr(0700,aesmd,aesmd) %{_rundir}/aesmd

%changelog
* Tue Nov 21 2023 Daniel P. Berrangé <berrange@redhat.com> - 2.22-1
- Initial packaging
