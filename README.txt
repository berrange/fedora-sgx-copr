Intel SGX software stack
========================

Build ordering is:

./coprbuild.sh sgx-srpm-macros

./coprbuild.sh sgx-emm:bootstrap
./coprbuild.sh sgx-dcap:bootstrap
./coprbuild.sh sgx-sdk:bootstrap
./coprbuild.sh sgx-ssl:bootstrap

./coprbuild.sh sgx-emm
./coprbuild.sh sgx-libcbor
./coprbuild.sh sgx-ipp-crypto
./coprbuild.sh sgx-rapidjson
./coprbuild.sh libcbor
./coprbuild.sh sgx-sdk
./coprbuild.sh sgx-dcap
./coprbuild.sh CppMicroServices
./coprbuild.sh sgx-psw

./coprbuild.sh sgx-mbedtls
./coprbuild.sh sgx-protobuf
./coprbuild.sh sgx-gperftools
./coprbuild.sh sgx-libomp


NB, ELN requires extra pre-requisites to be built first, using
src.rpm from Fedora rawhide
