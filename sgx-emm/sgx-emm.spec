# This package only provides SGX enclave content
%define debug_package %{nil}

# We need direct control over build flags used for the
# SGX enclave code.
%undefine _auto_set_build_flags

# Set to '1' if rebasing to a new version and do a full
# bootstrap of the SGX enclave stack
%{!?sgx_bootstrap:%define sgx_bootstrap 0}

Name: sgx-emm
Version: 1.0.2
Release: 1
License: BSD-3-Clause
Summary: SGX Enclave Memory Manager

Source0: https://github.com/intel/%{name}/archive/refs/tags/%{name}-%{version}.tar.gz
Source1: %{name}.make

BuildRequires: sgx-srpm-macros

%if %{sgx_bootstrap} == 0
BuildRequires: sgx-enclave-devel
BuildRequires: make
BuildRequires: gcc
%endif

%description
This is the implementation of the SGX Enclave Memory Manager.

%package -n sgx-enclave-emm-devel
Summary: SGX Enclave Memory Manager development libraries and headers

%if %{sgx_bootstrap} == 0
Requires: sgx-enclave-devel
%endif

%description -n sgx-enclave-emm-devel
This is the implementation of the SGX Enclave Memory Manager.

This package provides the enclave development libraries and headers.

%prep
%setup -n %{name}-%{name}-%{version}

cp %{SOURCE1} SGXMakefile

%build

%if %{sgx_bootstrap} == 0
%{make_build} -f SGXMakefile \
  SGX_SDK="%{sgx_prefix}" \
  ENCLAVE_CFLAGS="%{sgx_enclave_cflags}" \
  ENCLAVE_CXXFLAGS="%{sgx_enclave_cxxflags}" \
  ENCLAVE_LDFLAGS="%{sgx_enclave_ldflags}" \
  libsgx_mm.a
%endif

%install

%__install -d %{buildroot}%{sgx_includedir}

cp -a include %{buildroot}%{sgx_includedir}/%{name}

%if %{sgx_bootstrap} == 0

%__install -d %{buildroot}%{sgx_libdir}

cp libsgx_mm.a %{buildroot}%{sgx_libdir}/

%endif

%files -n sgx-enclave-emm-devel
%if %{sgx_bootstrap} == 0
%{sgx_libdir}/libsgx_mm.a
%endif

%dir %{sgx_includedir}/%{name}
%{sgx_includedir}/%{name}/*.h

%changelog
* Mon Jan 29 2024 Daniel P. Berrangé <berrange@redhat.com> - 1.0.2-1
- Initial packaging
