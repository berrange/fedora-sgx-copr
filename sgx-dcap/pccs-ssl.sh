#!/bin/sh

# Create a simple self-signed TLS cert for the PCCS server
# with expiry of 10 years. This is suitable for localhost
# usage only. If exposing on public IP addr, then real
# certs must be manually created at the $KEY and $CERT paths

SSL_DIR=/etc/pccs/ssl
KEY=$SSL_DIR/server-key.pem
CERT=$SSL_DIR/server-cert.pem
CSR=$SSL_DIR/server-cert.csr

if test -f $KEY || test -f $CERT
then
    exit 0
fi

openssl genrsa -out $KEY 2048
openssl req -new -key $KEY -out $CSR -subj "/CN=localhost"
openssl x509 -req -days 3650 -in $CSR -signkey $KEY -out $CERT
rm -f $CSR

chmod 0640 $KEY $CERT
chgrp pccs $KEY $CERT
